#!/bin/bash

# Print in specified color
print_color() {
    local color=$1
    local text=$2
    echo -e "\033[${color}m${text}\033[0m"
}

print_red() {
    print_color "31" "$1"
}

print_green() {
    print_color "32" "$1"
}

print_bold() {
    local text=$1
    echo -e "\033[1m${text}\033[0m"
}

exit_with_help() {
    print "Please provide two arguments: First argument should be the script name and second argument the directory path to process."
    exit 1
}

exit_with_error_message() {
    local message=$1
    print_red "$(print_bold "ERROR:") ${message}"
    exit 1
}

exit_with_success_message() {
    local message=$1
    print_green "$(print_bold "SUCCESS:") ${message}"
    exit 0
}

replace_empty_lines_with_temporary_strings() {
    local file_path=$1

    # Please note: The hash(#) is important here as using it to prepend the temporary strings makes 
    # yq intepret the string as a comment (which are ignored/not viewed as keys).
    # However sed interprets it purely as a string to match on which avoids indentation issues during the refactor.

    # Detect the operating system
    if [[ "$(uname)" == "Darwin" ]]; then
        # macOS (BSD) sed
        sed -i '' -e 's/^\s*$/#UNIQUE_KEY_TO_ISOLATE_CHANGES\:/' -e 's/[[:space:]]*$//' "${file_path}" || exit_with_error_message "Failed to replace empty lines with temporary strings ${file_path}"
    else
        # GNU sed and other POSIX-compliant versions
        sed -i -e 's/^\s*$/#UNIQUE_KEY_TO_ISOLATE_CHANGES:/' -e 's/[[:space:]]*$//' "$file_path" || exit_with_error_message "Failed to replace empty lines with temporary strings $file_path"
    fi
}

merge_key_into() {
    local file_path=$1
    local move_from_key=$2
    local move_to_key=$3

    yq eval "${move_to_key} += ${move_from_key} | del(${move_from_key})" -i ${file_path} || exit_with_error_message "Failed to merge keys in file ${file_path}"
}

delete_redundant_keys() {
    local file_path=$1

    yq eval 'del(._k8s.livenessProbe, ._version, ._k8s.readinessProbe, ._k8s.startupProbe)' -i "${file_path}" || exit_with_error_message "Failed to delete redundant keys in file ${file_path}"
}

delete_null_keys() {
    local file_path=$1
    yq eval 'del(.[][] | select(. == null))' -i "${file_path}" || exit_with_error_message "Failed to delete null keys in file ${file_path}"
}

remove_brackets() {
    local file_path=$1

    # Detect the operating system
    if [[ "$(uname)" == "Darwin" ]]; then
        # macOS (BSD) sed
        sed -i '' -e 's/{\s*}//g' -e 's/\[\s*\]//g' "${file_path}" || exit_with_error_message "Failed to remove brackets from file ${file_path}"
    else
        # GNU sed and other POSIX-compliant versions
        sed -i -e 's/{[[:space:]]*}//g' -e 's/\[[[:space:]]*\]//g' "${file_path}" || exit_with_error_message "Failed to remove brackets from file ${file_path}"
    fi
}

process_removal_of_temporary_strings_for_linebreaks() {
    local file_path=$1

    # Detect the operating system
    if [[ "$(uname)" == "Darwin" ]]; then
        # macOS (BSD) sed
        sed -i '' -e 's/\s*#NO_ONE_WILL_USE_THIS_KEY_EVER\:\s*//' -e 's/[[:space:]]*$//' "${file_path}" || exit_with_error_message "Failed to replace temporary strings lines with empty lines ${file_path}"
    else
        # GNU sed and other POSIX-compliant versions
        sed -i -e 's/\s*#NO_ONE_WILL_USE_THIS_KEY_EVER\:\s*//' -e 's/[[:space:]]*$//' "$file_path" || exit_with_error_message "Failed to replace temporary strings lines with empty lines $file_path"
    fi
}

process_single_yaml_file() {
    local file_path=$1

    # First completes a search for any empty lines and replaces them with a unique 
    # and temporary string value.
    replace_empty_lines_with_temporary_strings "${file_path}"

    # Executes a new search for the temporary string and deletes all the examples except 
    # for the (invisible to the IDE) linebreak character (\n).
    merge_key_into "${file_path}" "._version.image" "._setting.image"
    merge_key_into "${file_path}" "._k8s.livenessProbe" "._setting.livenessProbe"
    merge_key_into "${file_path}" "._k8s.readinessProbe" "._setting.readinessProbe"
    merge_key_into "${file_path}" "._k8s.startupProbe" "._setting.startupProbe"

    delete_redundant_keys "${file_path}"

    # The process of merging keys creates keys if they don't already exist in the file. 
    # These have no content and are added to the file as null. This function removes those.
    delete_null_keys "${file_path}"

    # The process of merging keys creates top level keys that inherit the structure of what yq expects the data to be.
    # This results in empty [] and {} being added after the script runs. These implied changes are not desired. 
    # This function removes those empty [] and {} changes, this command should run last as a cleanup.
    remove_brackets "${file_path}"

    process_removal_of_temporary_strings_for_linebreaks "${file_path}"
}

process_yaml_files_recursively() {
    local dir=$1
    local error_flag=false

    while IFS= read -r -d '' file; do
        if [[ ${file} == *.yaml || ${file} == *.yml ]]; then
            if ! process_single_yaml_file "${file}"; then
                print_red "Failed to process ${file}"
            else

                echo "Successfully processed: $(print_bold "${file}")"
            fi
        fi
    done < <(find "${dir}" -type f -print0)

    if $error_flag; then
        echo
        exit_with_error_message "An error occurred while processing YAML files in ${dir}"
    else
        echo
        exit_with_success_message "All YAML files in ${dir} processed successfully"
    fi
}

main() {
    if [ "$#" -ne 1 ]; then
       exit_with_help
    fi

    local dir_path=$1

    if [ ! -d "${dir_path}" ]; then
        exit_with_error_message "Directory ${dir_path} does not exist."
    fi

    process_yaml_files_recursively "${dir_path}"    
}

main "$@"
